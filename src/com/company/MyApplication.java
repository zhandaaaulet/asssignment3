package com.company;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class MyApplication {
    // users - a list of users
    private ArrayList<User> userList;
    private Scanner sc = new Scanner(System.in);
    private User signedUser;


    public MyApplication() throws FileNotFoundException {
        userList = new ArrayList<>();
        fillUsers();

    }

    public void fillUsers() throws FileNotFoundException {
        File file = new File("C:\\Users\\ACER\\IdeaProjects\\asssignment3\\src\\com\\company\\db.txt");
        Scanner fsc = new Scanner(file);
        while (fsc.hasNext()) userList.add(new User(fsc.nextInt(), fsc.next(), fsc.next(), fsc.next(), fsc.next()));
    }

    private void addUser(User user) {
        userList.add(user);

    }


    private void menu() throws IOException {
        while (true) {
            if (signedUser == null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Authentication");
                System.out.println("2. Exit");
                int choice = sc.nextInt();
                if (choice == 1) authentication();
                else break;
            } else {
                userProfile();
            }
        }
    }

    private void userProfile() throws FileNotFoundException, UnsupportedEncodingException {
        while (true) {
            System.out.println("Hello " + signedUser.getName() + " " + signedUser.getSurname());
            System.out.println("1) Log off");
            System.out.println("2) Create a group");

            int choice = sc.nextInt();
            if (choice == 1) {
                logOff();
                break;
            }
            else if(choice==2) {
                createGroup();
            }
        }

    }

    private void createGroup() {
        System.out.println("Please write the name of group: ");
        Scanner scanner = new Scanner(System.in);
        String groupName=scanner.next();
        System.out.println("Please choose the volume of the group:");
        int volume=scanner.nextInt();
        System.out.println("Please, choose the access of group: ");
        int access=scanner.nextInt();
        System.out.println("1. Private.");
        System.out.println("2. Public.");
        Group group = new Group();
        Admin admin= new Admin();
        admin.setAdmin(signedUser);
        group.setAccess(access==1);
        group.setGroupName(groupName);
        group.setVolume(volume);
        System.out.println("You have a create group.");
    }


    private void logOff() throws FileNotFoundException, UnsupportedEncodingException {
        signedUser = null;
    }

    private void authentication() throws FileNotFoundException, UnsupportedEncodingException {
        // sign in
        // sign up
        System.out.println("1) Sign in: ");
        System.out.println("2) Sign up: ");
        System.out.println("3) Exit");
        int choice = sc.nextInt();
        if (choice == 1) {
            signIn();

        } else if (choice == 2) {
            signUp();
        }
    }


    private void signIn() throws FileNotFoundException, UnsupportedEncodingException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Write your username: ");
        String username = sc.next();
        System.out.println("Write your password: ");

        Password password = new Password(sc.next());
        for (User user : userList) {
            if (username.equals(user.getUsername()) && password.equals(user.getPassword())) {
                signedUser = user;
                return;
            }
        }

        System.out.println("Incorrect password or username");
    }

    private void signUp() throws FileNotFoundException, UnsupportedEncodingException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Write your name: ");
        String name = sc.next();
        System.out.println("Write your surname: ");
        String surname = sc.next();
        System.out.println("Write your username: ");
        String username = sc.next();
        System.out.println("Write your password: ");
        String password = sc.next();

        if (name != null && surname != null && username != null && password != null) {
            User user = new User();
            user.setName(name);
            user.setSurname(surname);
            user.setUsername(username);
            user.setPassword(password);

            if (user.getPassword() != null) {
                addUser(user);
                signedUser = user;
                userProfile();
            }
        }
    }


    public void start() throws IOException {
        while (true) {
            System.out.println("Welcome to my application!");
            System.out.println("Select command:");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int choice = sc.nextInt();

            if (choice == 1) {
                menu();
            } else {
                break;
            }
        }

        // save the userlist to db.txt
        saveUserList();
    }

    private void saveUserList() throws IOException {
        String content = "";
        for (User user : userList) {
            content += user.getId() + " " + user.getName() + " " + user.getSurname() + " " +
                    user.getUsername() + " " + user.getPassword() + "\n";
        }

        Files.write(Paths.get("C:\\Users\\ACER\\IdeaProjects\\asssignment3\\src\\com\\company"), content.getBytes());

    }
}
