package com.company;

import java.util.LinkedList;

public class Admin extends User {
    private User admin;
    private LinkedList<Group> groups;


    public Admin(String name, String surname, String username, String password, Group groups) {
        super(name, surname, username, password);
        setGroups(groups);

    }

    public Admin() {

    }


    public LinkedList<Group> getGroups() {
        return groups;
    }

    public void setGroups(Group group) {
        this.groups = groups;
    }

    public User getAdmin() {
        return admin;
    }

    public void setAdmin(User admin) {
        this.admin = admin;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
