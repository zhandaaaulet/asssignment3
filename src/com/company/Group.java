package com.company;

import java.util.LinkedList;

public class Group {

    private Admin admin;
    private LinkedList<Subscriber> listSubscriber;
    public int volume;
    public boolean access = false;
    public String groupName;

    public Group() {

    }

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    public LinkedList<Subscriber> getListSubscriber() {
        return listSubscriber;
    }

    public void setListSubscriber(LinkedList<Subscriber> listSubscriber) {
        this.listSubscriber = listSubscriber;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public boolean isAccess() {
        return access;
    }

    public void setAccess(boolean access) {
        this.access = access;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public String toString() {
        return getGroupName() + " " + getAdmin() + " " + getVolume() + " " +
                ((getListSubscriber() == null) ? "no subscribers" : "have subscribesrs" +
                        " " + (isAccess() ? "private" : "public"));
    }
}
