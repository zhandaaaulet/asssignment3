package com.company;

public class User {
    // id (you need to generate this id by static member variable)
    // name, surname
    // username
    // password

    private int id;
    private static int id_gen = 1;
    private String name;
    private String surname;
    private String username;
    private Password password;

    public User() {
        this.id = ++id_gen;
    }

    public User(int id, String name, String surname, String username, String password) {
        setId(id);
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
    }

    public User(String name, String surname, String username, String password) {

        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
        if (id_gen < id + 1) {
            id_gen = id + 1;
        }

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Password getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = new Password(password);
    }


}
