package com.company;

public class Password {

    // passwordStr // it should contain uppercase and lowercase letters and digits
    // and its length must be more than 9 symbols
    private String password;

    public Password(String password) {
        setPassword(password);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        if (checkPassword(password)) {
            this.password = password;
        } else {
            System.out.println("Error : Incorrect password!");
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (this.getClass() != obj.getClass()) return false;
        Password pwdobj = (Password) obj;
        if (this.password.equals(pwdobj.password)) return true;
        return false;
    }

    @Override
    public int hashCode() {
        return password != null ? password.hashCode() : 0;
    }

    private static boolean checkPassword(String userPassword) {

        boolean hasDigit = false;
        boolean hasUpperCase = false;
        boolean hasLowerCase = false;



        if (userPassword.length() >9) {

            for (int i = 0; i < userPassword.length(); i++) {
                char currentChar = userPassword.charAt(i);

                if ( Character.isUpperCase(currentChar)) {
                    hasUpperCase = true;
                } else if (Character.isLowerCase(currentChar)) {
                    hasLowerCase = true;
                } else if (Character.isDigit(currentChar)) {
                    hasDigit = true;
                }
            }
            return hasDigit && hasUpperCase && hasLowerCase;
        } else {
            return false;
        }
    }



    @Override
    public String toString() {
        return password;
    }
}

